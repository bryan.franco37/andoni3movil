﻿using System.Collections.Generic;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class Section
    {
        public Section()
        {
            SubSection = new HashSet<SubSection>();
        }

        public int Id { get; set; }
        public int? AssessmentId { get; set; }
        public string Description { get; set; }

        public virtual Assessment Assessment { get; set; }
        public virtual ICollection<SubSection> SubSection { get; set; }
    }
}
