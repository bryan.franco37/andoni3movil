﻿
namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class ResponseQuestion
    {
        public int Id { get; set; }
        public int? ResponseId { get; set; }
        public int? QuestionId { get; set; }
        public int? OptionId { get; set; }
        public string Comments { get; set; }

        public virtual Option Option { get; set; }
        public virtual Question Question { get; set; }
        public virtual Response Response { get; set; }
    }
}
