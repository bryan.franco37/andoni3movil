﻿using Microsoft.EntityFrameworkCore;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class TegraSelfAssessmentContext : DbContext
    {
        public TegraSelfAssessmentContext()
        {
        }

        public TegraSelfAssessmentContext(DbContextOptions<TegraSelfAssessmentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<Assessment> Assessment { get; set; }
        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<Option> Option { get; set; }
        public virtual DbSet<Question> Question { get; set; }
        public virtual DbSet<QuestionOption> QuestionOption { get; set; }
        public virtual DbSet<QuestionRequirement> QuestionRequirement { get; set; }
        public virtual DbSet<Requirement> Requirement { get; set; }
        public virtual DbSet<Response> Response { get; set; }
        public virtual DbSet<ResponseAssessment> ResponseAssessment { get; set; }
        public virtual DbSet<ResponseImage> ResponseImage { get; set; }
        public virtual DbSet<ResponseQuestion> ResponseQuestion { get; set; }
        public virtual DbSet<RoleClaims> RoleClaims { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<SubSection> SubSection { get; set; }
        public virtual DbSet<UserClaims> UserClaims { get; set; }
        public virtual DbSet<UserLogins> UserLogins { get; set; }
        public virtual DbSet<UserRoles> UserRoles { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DEX-ARGUELLO;Database=Tegra.SelfAssessment;Trusted_Connection=True;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.1-servicing-10028");

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Assessment>(entity =>
            {
                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Number).IsUnicode(false);

                entity.HasOne(d => d.SubSection)
                    .WithMany(p => p.Group)
                    .HasForeignKey(d => d.SubSectionId)
                    .HasConstraintName("FK_Group_SubSection");
            });

            modelBuilder.Entity<Option>(entity =>
            {
                entity.Property(e => e.Statement).IsUnicode(false);
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.Property(e => e.Statement).IsUnicode(false);

                entity.HasOne(d => d.FatherQuestion)
                    .WithMany(p => p.InverseFatherQuestion)
                    .HasForeignKey(d => d.FatherQuestionId)
                    .HasConstraintName("FK_Question_Question");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Question)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_Question_Group");
            });

            modelBuilder.Entity<QuestionOption>(entity =>
            {
                entity.HasOne(d => d.Option)
                    .WithMany(p => p.QuestionOption)
                    .HasForeignKey(d => d.OptionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionOption_Options");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.QuestionOption)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionOption_Question");
            });

            modelBuilder.Entity<QuestionRequirement>(entity =>
            {
                entity.HasKey(e => new { e.QuestionId, e.RequirementId });

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.QuestionRequirement)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionRequirement_Question");

                entity.HasOne(d => d.Requirement)
                    .WithMany(p => p.QuestionRequirement)
                    .HasForeignKey(d => d.RequirementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionRequirement_Requirement");
            });

            modelBuilder.Entity<Requirement>(entity =>
            {
                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<Response>(entity =>
            {
                entity.Property(e => e.ActiveEndDate).HasColumnType("date");

                entity.Property(e => e.ActiveStartDate).HasColumnType("date");

                entity.Property(e => e.Crcode).HasColumnName("CRCode");

                entity.HasOne(d => d.Assessment)
                    .WithMany(p => p.Response)
                    .HasForeignKey(d => d.AssessmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Response_AssessmentAssessment");
            });

            modelBuilder.Entity<ResponseAssessment>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Crcode).HasColumnName("CRCode");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.ResponseAssessment)
                    .HasForeignKey<ResponseAssessment>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ResponseAssessment_Response");
            });

            modelBuilder.Entity<ResponseImage>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FileName).IsUnicode(false);

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.ResponseImage)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ResponseImage_Question");

                entity.HasOne(d => d.Response)
                    .WithMany(p => p.ResponseImage)
                    .HasForeignKey(d => d.ResponseId)
                    .HasConstraintName("FK_ResponseImage_Response");
            });

            modelBuilder.Entity<ResponseQuestion>(entity =>
            {
                entity.Property(e => e.Comments).IsUnicode(false);

                entity.HasOne(d => d.Option)
                    .WithMany(p => p.ResponseQuestion)
                    .HasForeignKey(d => d.OptionId)
                    .HasConstraintName("FK_ResponseQuestion_Option");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.ResponseQuestion)
                    .HasForeignKey(d => d.QuestionId)
                    .HasConstraintName("FK_ResponseQuestion_Question");

                entity.HasOne(d => d.Response)
                    .WithMany(p => p.ResponseQuestion)
                    .HasForeignKey(d => d.ResponseId)
                    .HasConstraintName("FK_ResponseQuestion_Response");
            });

            modelBuilder.Entity<RoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<Section>(entity =>
            {
                entity.HasIndex(e => e.AssessmentId);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.HasOne(d => d.Assessment)
                    .WithMany(p => p.Section)
                    .HasForeignKey(d => d.AssessmentId)
                    .HasConstraintName("FK_Section_Assessment");
            });

            modelBuilder.Entity<SubSection>(entity =>
            {
                entity.HasIndex(e => e.SectionId);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.SubSection)
                    .HasForeignKey(d => d.SectionId)
                    .HasConstraintName("FK_SubSection_Sections");
            });

            modelBuilder.Entity<UserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<UserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<UserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });
        }
    }
}
