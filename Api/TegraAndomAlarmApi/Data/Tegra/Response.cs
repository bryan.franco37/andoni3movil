﻿using System;
using System.Collections.Generic;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class Response
    {
        public Response()
        {
            ResponseImage = new HashSet<ResponseImage>();
            ResponseQuestion = new HashSet<ResponseQuestion>();
        }

        public int Id { get; set; }
        public int AssessmentId { get; set; }
        public string Crcode { get; set; }
        public string FactoryName { get; set; }
        public string FactoryContactName { get; set; }
        public string FactoryAssessorName { get; set; }
        public string OtherAssessorsNames { get; set; }
        public DateTime? ActiveStartDate { get; set; }
        public DateTime? ActiveEndDate { get; set; }
        public bool Active { get; set; }

        public virtual Assessment Assessment { get; set; }
        public virtual ResponseAssessment ResponseAssessment { get; set; }
        public virtual ICollection<ResponseImage> ResponseImage { get; set; }
        public virtual ICollection<ResponseQuestion> ResponseQuestion { get; set; }
    }
}
