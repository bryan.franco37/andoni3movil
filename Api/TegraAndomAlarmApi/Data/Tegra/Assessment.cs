﻿using System.Collections.Generic;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class Assessment
    {
        public Assessment()
        {
            Response = new HashSet<Response>();
            Section = new HashSet<Section>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Response> Response { get; set; }
        public virtual ICollection<Section> Section { get; set; }
    }
}
