﻿using System.Collections.Generic;

namespace Tegra.SelfAssessmentAPI.Data
{
    public partial class Requirement
    {
        public Requirement()
        {
            QuestionRequirement = new HashSet<QuestionRequirement>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<QuestionRequirement> QuestionRequirement { get; set; }
    }
}
