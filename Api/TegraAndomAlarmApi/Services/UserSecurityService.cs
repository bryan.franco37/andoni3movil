﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Tegra.Models;
using Tegra.SelfAssessmentAPI.Data;

namespace Tegra.Services
{
    public class UserSecurityService
    {
        private IConfiguration _conf;
        private UserManager<Users> _userManager;

        private readonly string _issuer;
        private readonly string _audience;

        private string _errors;
        private int _jwtexpirationtime = 1;

        public UserSecurityService(IConfiguration conf, UserManager<Users> userManager)
        {
            _conf = conf;
            _userManager = userManager;
            _issuer = conf.GetValue<string>("Tokens:issuer");
            _audience = conf.GetValue<string>("Tokens:audience");
        }

        public async Task<AuthenticationResult> Auth(string email, string password)
        {
            try
            {
                string error = string.Empty;

                var passwordValidator = new PasswordValidator<Users>();
                var user = await _userManager.FindByNameAsync(email);
                var result = await passwordValidator.ValidateAsync(_userManager, user, password);
                if (result.Succeeded)
                {
                    // generate JWT
                    var claims = new[] {
                        new Claim("TegraAndomAlarmAPI", email)
                    };
                    var tokenJWT = GenerateJWT(_jwtexpirationtime, email, claims);
                    if (tokenJWT != null)
                        return new AuthenticationResult { JWT = new JwtSecurityTokenHandler().WriteToken(tokenJWT), JWTExpiration = tokenJWT.ValidTo, Success = true, Error = string.Empty };
                    else
                        error = "Error generating JWT " + _errors;
                }
                else
                {
                    error = "Invalid credentials";
                }
                return new AuthenticationResult { Error = error, Success = false };
            }
            catch (Exception ex)
            {
                _errors = ex.Message;
                return new AuthenticationResult { Success = false, Error = ex.Message };
            }
        }

        public async Task<AuthenticationResult> Auth(string token)
        {
            try
            {
                string error = string.Empty;
                var email = await GetEmailFromToken(token);
                if (!string.IsNullOrEmpty(email))
                {
                    // generate JWT
                    var claims = new[] {
                        new Claim("SelfAssessmentAPI", email)
                    };
                    var tokenJWT = GenerateJWT(_jwtexpirationtime, email, claims);
                    if (tokenJWT != null)
                        return new AuthenticationResult { JWT = new JwtSecurityTokenHandler().WriteToken(tokenJWT), JWTExpiration = tokenJWT.ValidTo, Success = true, Error = string.Empty };
                    else
                        error = "Error generating JWT " + _errors;
                }
                else
                {
                    error = "Invalid credentials";
                }
                return new AuthenticationResult { Error = error, Success = false };
            }
            catch (Exception ex)
            {
                _errors = ex.Message;
                return new AuthenticationResult { Success = false, Error = ex.Message };
            }
        }

        private async Task<string> GetEmailFromToken(string token)
        {
            var r = string.Empty;
            try
            {
                var graphResourceUri = _conf.GetValue<string>("AzureActiveDirectory:graphResourceUri");
                var tenantId = _conf.GetValue<string>("AzureActiveDirectory:tenantId");
                var graphApiVersion = _conf.GetValue<string>("AzureActiveDirectory:graphApiVersion");

                string graphRequest = String.Format(CultureInfo.InvariantCulture, "{0}/{1}/me?api-version={2}", graphResourceUri, tenantId, graphApiVersion);
                HttpClient client = new HttpClient();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, graphRequest);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                HttpResponseMessage response = await client.SendAsync(request);

                string content = await response.Content.ReadAsStringAsync();
                var jResult = JObject.Parse(content);

                r = jResult["userPrincipalName"].ToString();
            }
            catch (Exception)
            {

            }
            return r;
        }

        public JwtSecurityToken GenerateJWT(int expireMinutes, string email, Claim[] jwtclaims)
        {
            var cert = Path.Combine(Directory.GetCurrentDirectory(), "Certificates", "CA_Certificate.pfx");
            try
            {
                var claims = new[] {
                    new Claim (JwtRegisteredClaimNames.Sub, email),
                    new Claim (JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }.Union(jwtclaims);

                X509Certificate2 singninCert = new X509Certificate2(cert, "rhwp7686", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);
                X509SecurityKey privateKey = new X509SecurityKey(singninCert);
                var credentials = new SigningCredentials(privateKey, SecurityAlgorithms.RsaSha256);

                var token = new JwtSecurityToken(
                    issuer: _issuer,
                    audience: _audience,
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(expireMinutes),
                    signingCredentials: credentials
                );

                return token;
            }
            catch (Exception ex)
            {
                _errors = $"{ex.Message} {ex.StackTrace} {cert}";
                return null;
            }
        }

    }
}
