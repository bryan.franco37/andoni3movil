﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tegra.SelfAssessmentAPI.Data;

namespace Tegra.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AssessmentController : ControllerBase
    {

        private readonly TegraSelfAssessmentContext _context;
        public AssessmentController(TegraSelfAssessmentContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {

            var list = _context
                .Response
                .Include(x => x.Assessment)
                .ThenInclude(x => x.Section)
                .ThenInclude(x => x.SubSection)
                .ThenInclude(x => x.Group)
                .ThenInclude(x => x.Question)
                .ThenInclude(x => x.ResponseQuestion);

            var response = new List<AssessmentCard>();
            foreach (var o in list)
            {
                var answeredCount = o.Assessment.Section.Sum(x => x.SubSection.Sum(a => a.Group.Sum(b => b.Question.Sum(c => c.ResponseQuestion.Count(d => d.OptionId != null)))));

                var questionCount = o.Assessment.Section.Sum(x => x.SubSection.Sum(a => a.Group.Sum(b => b.Question.Count())));

                var color = "Green";
                if (answeredCount != questionCount) color = "Red";

                response.Add(new AssessmentCard
                {
                    ResponseId = o.Id,
                    Name = o.Assessment.Name,

                    AssessmentId = o.AssessmentId,
                    Crcode = o.Crcode,
                    FactoryName = o.FactoryName,
                    FactoryContactName = o.FactoryContactName,
                    FactoryAssessorName = o.FactoryAssessorName,
                    OtherAssessorsNames = o.OtherAssessorsNames,
                    ActiveStartDate = o.ActiveStartDate?.ToString("dd/MM/yyyy"),
                    ActiveEndDate = o.ActiveEndDate?.ToString("dd/MM/yyyy"),

                    AnsweredQuestion = $"Answered questions: {answeredCount}/{questionCount}",
                    AnsweredQuestionColor = color
                });
            }

            return Ok(response);
        }

        public class AssessmentCard
        {
            public int ResponseId { get; set; }
            public string Name { get; set; }

            public int AssessmentId { get; set; }
            public string AssessmentDate { get; set; }
            public string Crcode { get; set; }
            public string FactoryName { get; set; }
            public string FactoryContactName { get; set; }
            public string FactoryAssessorName { get; set; }
            public string OtherAssessorsNames { get; set; }
            public string ActiveStartDate { get; set; }
            public string ActiveEndDate { get; set; }

            public string AnsweredQuestion { get; set; }
            public string AnsweredQuestionColor { get; set; }
        }

    }
}
