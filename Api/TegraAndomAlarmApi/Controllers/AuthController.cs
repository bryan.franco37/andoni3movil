﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Tegra.Models;
using Tegra.SelfAssessmentAPI.Data;
using Tegra.Services;

namespace Tegra.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IConfiguration _conf;
        private ILogger _logger;
        private UserManager<Users> _userManager;

        public AuthController(IConfiguration conf, ILogger<AuthController> logger, UserManager<Users> userManager)
        {
            _conf = conf;
            _logger = logger;
            _userManager = userManager;
        }

        [HttpGet("{email}/{password}")]
        public async Task<ActionResult> PostData(string email, string password)
        {
            try
            {
                UserSecurityService security = new UserSecurityService(_conf, _userManager);
                AuthenticationResult result = await security.Auth(email, password);

                if (result.Success)
                    return Ok(new
                    {
                        Success = true,
                        token = result.JWT,
                        expiration = result.JWTExpiration
                    });
                else
                    return BadRequest(new { Success = false, Message = result.Error });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception throw while login: {ex.Message}");
                return BadRequest("Failed to generate token");
            }
        }

        [HttpPost]
        public async Task<ActionResult> PostData([FromBody] string token)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new { Success = false, Message = "Provide user credentials. " });
                }

                UserSecurityService security = new UserSecurityService(_conf, null);
                AuthenticationResult result = await security.Auth(token);

                if (result.Success)
                    return Ok(new
                    {
                        Success = true,
                        token = result.JWT,
                        expiration = result.JWTExpiration
                    });
                else
                    return BadRequest(new { Success = false, Message = result.Error });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception throw while login: {ex.Message}");
                return BadRequest("Failed to generate token");
            }
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(true);
        }

    }
}