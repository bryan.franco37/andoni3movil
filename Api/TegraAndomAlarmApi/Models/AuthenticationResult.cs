﻿using System;

namespace Tegra.Models
{
    public class AuthenticationResult
    {
        public bool Success { get; set; }

        public string Error { get; set; }

        public string JWT { get; set; }

        public DateTime JWTExpiration { get; set; }

    }
}
