﻿using AndonAlarm.Common;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AndonAlarm.Views
{
    public partial class MainPage : ContentPage
    {
        private HubConnection _connection;
        
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            string userName = string.Empty;
            if (App.Current.Properties.ContainsKey("UserName"))
            {
                userName = $"{(string)App.Current.Properties["UserName"]}";
            }
            UserNameLabel.Text = userName;
        }

        protected async override void OnAppearing()
        {
            Refresh();

            try
            {
                MessagingCenter.Subscribe<Message>(this, "ValidToken", (message) => {
                    if (!message.Value)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            LogoutButton_OnClicked(null, null);
                        });
                    }
                });

                MessagingCenter.Subscribe<Message>(this, "Enabled", (message) => {
                    if (message.Value)
                    {
                        Device.BeginInvokeOnMainThread(() => { LineasGrid.IsEnabled = true; });
                    }
                });

                _connection = new HubConnectionBuilder()
                    .WithUrl($"{Constants.Url}/andon/alerts")
                    .Build();

                _connection.Closed += async (error) =>
                {
                    await Task.Delay(new Random().Next(0, 5) * 1000);
                    await _connection.StartAsync();
                };

                _connection.On<int, AndonAlert>("ReceiveAlert", (id, obj) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        var exists = false;
                        foreach (var child in AlarmFlexLayout.Children)
                        {
                            var frame = (Frame)child;
                            var box = (Frame)frame.Children.FirstOrDefault();
                            var label = (Label)box.Children.FirstOrDefault();
                            var productionLineDto = (ProductionLineDto)(label.BindingContext);

                            if (id == productionLineDto.Id)
                            {
                                if (obj.DrawAndonAlertTypeId == 1)
                                {
                                    productionLineDto.Alert = new AndonAlert { AndonAlertId = obj.AndonAlertId, AndonAlertTypeId = obj.AndonAlertTypeId };
                                    label.BindingContext = productionLineDto;

                                    box.BackgroundColor = Color.FromRgb(216, 63, 69);
                                    box.BorderColor = Color.Transparent;
                                    label.BackgroundColor = Color.FromRgb(216, 63, 69);
                                    label.TextColor = Color.White;
                                }
                                else
                                {
                                    var delay = obj.AlarmDelaySeconds;
                                    if (delay > 0)
                                    {
                                        Task t = new Task(() =>
                                        {
                                            Device.BeginInvokeOnMainThread(async () =>
                                            {
                                                await Task.Delay(delay * 1000);
                                                box.BackgroundColor = Color.FromRgb(216, 63, 69);
                                                box.BorderColor = Color.Transparent;
                                                label.BackgroundColor = Color.FromRgb(216, 63, 69);
                                                label.TextColor = Color.White;
                                            });
                                        });
                                        t.Start();
                                    }

                                    productionLineDto.Alert = new AndonAlert { AndonAlertId = obj.AndonAlertId, AndonAlertTypeId = obj.AndonAlertTypeId };
                                    label.BindingContext = productionLineDto;

                                    box.BackgroundColor = Color.FromRgb(247, 134, 25);
                                    box.BorderColor = Color.Transparent;
                                    label.BackgroundColor = Color.FromRgb(247, 134, 25);
                                    label.TextColor = Color.White;
                                }

                                exists = true;
                                break;
                            }
                        }
                        if (!exists)
                        {
                            Refresh();
                        }
                    });
                });

                _connection.On<int>("RemoveAlert", (id) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        foreach (var child in AlarmFlexLayout.Children)
                        {
                            var frame = (Frame)child;
                            var box = (Frame)frame.Children.FirstOrDefault();
                            var label = (Label)box.Children.FirstOrDefault();
                            var productionLineDto = (ProductionLineDto)(label.BindingContext);

                            if (id == productionLineDto.Id)
                            {
                                productionLineDto.Alert = null;
                                label.BindingContext = productionLineDto;

                                box.BackgroundColor = Color.FromRgb(92, 161, 92);
                                box.BorderColor = Color.Transparent;
                                label.BackgroundColor = Color.FromRgb(92, 161, 92);
                                label.TextColor = Color.White;

                                Task t = new Task(() =>
                                {
                                    Device.BeginInvokeOnMainThread(async () =>
                                    {
                                        await Task.Delay(10 * 1000);
                                        box.BackgroundColor = Color.FromRgb(92, 161, 92);
                                        box.BorderColor = Color.Transparent;
                                        label.BackgroundColor = Color.FromRgb(92, 161, 92);
                                        label.TextColor = Color.White;
                                    });
                                });
                                t.Start();
                            }
                        }
                    });
                });

                await _connection.StartAsync();
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                if (ex.InnerException != null) error += " " + ex.InnerException.Message;
                await DisplayAlert("Alert", error, "OK");
            }

        }

        protected override bool OnBackButtonPressed() => true;

        private async void LogoutButton_OnClicked(object sender, EventArgs e)
        {
            //Delete Azure Auth
            AuthenticationContext ac = new AuthenticationContext(Constants.Authority);
            ac.TokenCache.Clear();
            var auth = DependencyService.Get<IAndroidService>();
            auth.ClearAllCookies();

            //Delete Properties
            App.Current.Properties.Clear();

            //Redirect
            var page = new LoginPage();
            await Navigation.PushAsync(page);
        }

        private async void Refresh()
        {
            try
            {
                AlarmFlexLayout.Children.Clear();

                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                using (var client = new HttpClient(new HttpClientHandler()))
                {
                    string tokenApi = string.Empty;
                    if (App.Current.Properties.ContainsKey("TokenAPI"))
                    {
                        tokenApi = (string)App.Current.Properties["TokenAPI"];
                    }

                    client.Timeout = new TimeSpan(0, 0, 10);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                    var response = await client.GetAsync($"{Constants.Url}/Api/user/productionLines/");
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        var list = JsonConvert.DeserializeObject<List<ProductionLineDto>>(content);

                        foreach (var p in list)
                        {

                            Frame frame = new Frame
                            {
                                BackgroundColor = Color.Transparent,
                                Padding = new Thickness(5, 5, 5, 5)
                            };

                            Frame box = new Frame
                            {
                                BackgroundColor = Color.FromRgb(92, 161, 92),
                                BorderColor = Color.Transparent,
                                Padding = new Thickness(5, 5, 5, 5),
                                CornerRadius = 10
                            };

                            Label label = new Label
                            {
                                Text = $"{p.Name}",
                                BackgroundColor = Color.FromRgb(92, 161, 92),
                                TextColor = Color.White
                            };
                            if (p.Alert != null)
                            {
                                if (p.Alert.DrawAndonAlertTypeId == 1)
                                {
                                    box.BackgroundColor = Color.FromRgb(216, 63, 69);
                                    box.BorderColor = Color.Transparent;
                                    label.BackgroundColor = Color.FromRgb(216, 63, 69);
                                    label.TextColor = Color.White;
                                }
                                else
                                {
                                    var delay = p.Alert.AlarmDelaySeconds;
                                    if (delay > 0)
                                    {
                                        Task t = new Task(() =>
                                        {
                                            Device.BeginInvokeOnMainThread(async () =>
                                            {
                                                await Task.Delay(delay * 1000);
                                                box.BackgroundColor = Color.FromRgb(216, 63, 69);
                                                box.BorderColor = Color.Transparent;
                                                label.BackgroundColor = Color.FromRgb(216, 63, 69);
                                                label.TextColor = Color.White;
                                            });
                                        });
                                        t.Start();
                                    }

                                    box.BackgroundColor = Color.FromRgb(247, 134, 25);
                                    box.BorderColor = Color.Transparent;
                                    label.BackgroundColor = Color.FromRgb(247, 134, 25);
                                    label.TextColor = Color.White;
                                }
                            }
                            label.WidthRequest = 100;
                            label.HeightRequest = 100;
                            label.HorizontalTextAlignment = TextAlignment.Center;
                            label.VerticalTextAlignment = TextAlignment.Center;

                            label.BindingContext = p;

                            var tap = new TapGestureRecognizer();
                            tap.Tapped += (s, e) =>
                            {
                                var lbl = (Label)s;
                                var bin = (ProductionLineDto)lbl.BindingContext;
                                LineasGrid.IsEnabled = false;
                                PopupNavigation.Instance.PushAsync(new AlertPage(bin));
                            };
                            label.GestureRecognizers.Add(tap);

                            box.Content = label;
                            frame.Content = box;
                            AlarmFlexLayout.Children.Add(frame);

                        }

                    }
                    else
                    {
                        JObject json = JObject.Parse(await response.Content.ReadAsStringAsync());
                        await DisplayAlert("Alert", json["message"].ToString(), "OK");
                    }
                }

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                if (ex.InnerException != null) error += " " + ex.InnerException.Message;
                await DisplayAlert("Alert", error, "OK");
                LogoutButton_OnClicked(null, null);
            }
        }

    }

}
