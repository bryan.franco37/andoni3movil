﻿using AndonAlarm.Common;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AndonAlarm.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlertPage : PopupPage
    {
        private ProductionLineDto ProductionLineDto { get; set; }

        public AlertPage(ProductionLineDto productionLineDto)
        {
            InitializeComponent();

            ProductionLineDto = productionLineDto;

            if (ProductionLineDto.Alert == null)
            {
                Comment.IsVisible = true;
                CommentLabel.IsVisible = true;
                AlertButton.IsVisible = true;
                WarningButton.IsVisible = true;
                CompleteButton.IsVisible = false;
            }
            else if (ProductionLineDto.Alert.DrawAndonAlertTypeId == 1)
            {
                Comment.IsVisible = false;
                CommentLabel.IsVisible = false;
                AlertButton.IsVisible = false;
                WarningButton.IsVisible = false;
                CompleteButton.IsVisible = true;
            }
            else if (ProductionLineDto.Alert.DrawAndonAlertTypeId == 2)
            {
                Comment.IsVisible = false;
                CommentLabel.IsVisible = false;
                AlertButton.IsVisible = true;
                WarningButton.IsVisible = false;
                CompleteButton.IsVisible = true;
            }
        }

        private async void AlertButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                AlertButton.IsEnabled = false;
                var alert = new AlertDto
                {
                    ProductionLineId = ProductionLineDto.Id,
                    TypeId = 1, Comment = Comment.Text
                };
                ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
                using (var client = new HttpClient(new HttpClientHandler()))
                {
                    string tokenApi = string.Empty;
                    if (App.Current.Properties.ContainsKey("TokenAPI"))
                    {
                        tokenApi = (string)App.Current.Properties["TokenAPI"];
                    }

                    client.Timeout = new TimeSpan(0, 0, 10);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                    var res = await client.PostAsJsonAsync($"{Constants.Url}/api/andon/alert/", alert);
                    if (res.IsSuccessStatusCode)
                    {
                        AlertButton.IsEnabled = true;
                    }
                    else
                    {
                        MessagingCenter.Send(new Message { Value = false }, "ValidToken");
                    }
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
                MessagingCenter.Send(new Message { Value = false }, "ValidToken");
            }
            MessagingCenter.Send(new Message { Value = true }, "Enabled");
            await PopupNavigation.Instance.PopAsync();
        }

        private async void WarningButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                WarningButton.IsEnabled = false;
                var alert = new AlertDto
                {
                    ProductionLineId = ProductionLineDto.Id,
                    TypeId = 2,
                    Comment = Comment.Text
                };
                ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
                using (var client = new HttpClient(new HttpClientHandler()))
                {
                    string tokenApi = string.Empty;
                    if (App.Current.Properties.ContainsKey("TokenAPI"))
                    {
                        tokenApi = (string)App.Current.Properties["TokenAPI"];
                    }

                    client.Timeout = new TimeSpan(0, 0, 10);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                    var res = await client.PostAsJsonAsync($"{Constants.Url}/api/andon/alert/", alert);
                    if (res.IsSuccessStatusCode)
                    {
                        WarningButton.IsEnabled = true;
                    }
                    else
                    {
                        MessagingCenter.Send(new Message { Value = false }, "ValidToken");
                    }

                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
                MessagingCenter.Send(new Message { Value = false }, "ValidToken");
            }
            MessagingCenter.Send(new Message { Value = true }, "Enabled");
            await PopupNavigation.Instance.PopAsync();
        }

        private async void CompleteButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                CompleteButton.IsEnabled = false;
                ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
                using (var client = new HttpClient(new HttpClientHandler()))
                {
                    string tokenApi = string.Empty;
                    if (App.Current.Properties.ContainsKey("TokenAPI"))
                    {
                        tokenApi = (string)App.Current.Properties["TokenAPI"];
                    }

                    client.Timeout = new TimeSpan(0, 0, 10);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenApi);
                    var res = await client.DeleteAsync($"{Constants.Url}/Api/andon/alert/{ProductionLineDto.Alert.AndonAlertId}");

                    CompleteButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", ex.Message, "OK");
                MessagingCenter.Send(new Message { Value = false }, "ValidToken");
            }
            MessagingCenter.Send(new Message { Value = true }, "Enabled");
            await PopupNavigation.Instance.PopAsync();
        }

        protected override bool OnBackgroundClicked()
        {
            MessagingCenter.Send(new Message { Value = true }, "Enabled");
            return base.OnBackgroundClicked();
        }

    }
}