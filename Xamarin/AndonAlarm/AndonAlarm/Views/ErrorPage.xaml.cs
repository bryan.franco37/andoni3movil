﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AndonAlarm.Views
{

	public partial class ErrorPage : ContentPage
    {

        public class MyViewModel
        {
            public MyViewModel(string text)
            {
                MyProperty = text;
            }

            public string MyProperty { get; set; }
        }

        public ErrorPage(string error)
		{
			InitializeComponent ();

		    this.BindingContext = new MyViewModel(error);
        }
	}
}