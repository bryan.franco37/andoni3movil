﻿using AndonAlarm.Common;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AndonAlarm.Views
{
    public partial class LoginPage : ContentPage
    {

        private AuthenticationResult authResult = null;

        public LoginPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override bool OnBackButtonPressed() => true;

        private async void LoginButton_OnClicked(object sender, EventArgs e)
        {
            LoginButton.IsEnabled = false;
            await TokenEmailValidation();
        }

        private async void TegraButton_OnClicked(object sender, EventArgs e)
        {
            TegraButton.IsEnabled = false;
            await TokenAzureValidation();
        }


        private async Task TokenEmailValidation()
        {
            //API TOKEN 
            var tokenApi = await GetTokenAPI($"{Constants.Url}/Api", UsernameEditor.Text, PasswordEditor.Text);
            if (!string.IsNullOrEmpty(tokenApi))
            {
                App.Current.Properties.Add("UserName", UsernameEditor.Text);
                App.Current.Properties.Add("TokenAPI", tokenApi);
                await App.Current.SavePropertiesAsync();

                var page = new MainPage();
                await Navigation.PushAsync(page);
            }
        }

        private async Task TokenAzureValidation()
        {
            //AZURE TOKEN 
            string tokenAzure = string.Empty;
            if (App.Current.Properties.ContainsKey("TokenAzure"))
            {
                tokenAzure = (string)App.Current.Properties["TokenAzure"]; 
            }
            else
            {
                tokenAzure = await GetTokenAzure();
            }

            //API TOKEN 
            var tokenApi = await GetTokenAPI($"{Constants.Url}/Api", tokenAzure);
            if (!string.IsNullOrEmpty(tokenApi))
            {
                App.Current.Properties.Add("TokenAPI", tokenApi);
                await App.Current.SavePropertiesAsync();

                var page = new MainPage();
                await Navigation.PushAsync(page);
            }
        }

        private async Task<string> GetTokenAzure()
        {
            var auth = DependencyService.Get<IAndroidService>();
            authResult = await auth.Authenticate(Constants.Authority, Constants.GraphResourceUri, Constants.ClientId, Constants.ReturnUri);

            App.Current.Properties.Add("TokenAzure", authResult.AccessToken);
            App.Current.Properties.Add("UserName", authResult.UserInfo.DisplayableId);
            await App.Current.SavePropertiesAsync();

            return authResult.AccessToken;
        }

        public async Task<string> GetTokenAPI(string apiUrl, string username, string password)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            using (var client = new HttpClient(new HttpClientHandler()))
            {
                var cred = new CredentialsDto()
                {
                    Username = username.Trim(),
                    Password = password.Trim()
                };
                client.Timeout = new TimeSpan(0, 0, 10);
                try
                {
                    var response = await client.PostAsJsonAsync($"{apiUrl}/auth/token", cred);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        var jResult = JObject.Parse(content);
                        App.Current.Properties.Add("EmailValidation", false);
                        return jResult["token"].ToString();
                    }
                    else
                    {
                        JObject json = JObject.Parse(await response.Content.ReadAsStringAsync());
                        await DisplayAlert("Alert", json["message"].ToString(), "OK");
                    }
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    if (ex.InnerException != null) error += " " + ex.InnerException.Message;
                    await DisplayAlert("Alert", error, "OK");
                }
                LoginButton.IsEnabled = true;
                return string.Empty;
            }
        }

        public async Task<string> GetTokenAPI(string apiUrl, string tokenAzure)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            using (var client = new HttpClient(new HttpClientHandler()))
            {
                client.Timeout = new TimeSpan(0, 0, 10);
                try
                {
                    var response = await client.PostAsJsonAsync($"{apiUrl}/auth/", tokenAzure);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        var jResult = JObject.Parse(content);
                        App.Current.Properties.Add("AzureValidation", true);
                        return jResult["token"].ToString();
                    }
                    else
                    {
                        JObject json = JObject.Parse(await response.Content.ReadAsStringAsync());
                        await DisplayAlert("Alert", json["message"].ToString(), "OK");
                    }
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    if (ex.InnerException != null) error += " " + ex.InnerException.Message;
                    await DisplayAlert("Alert", error, "OK");
                }
                TegraButton.IsEnabled = true;
                return string.Empty;
            }
        }

    }
}
