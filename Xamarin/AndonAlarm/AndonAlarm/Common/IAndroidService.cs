﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Threading.Tasks;

namespace AndonAlarm.Common
{
    public interface IAndroidService
    {

        string GetExternalFilesPath();
        Task SaveImage(byte[] reducedImage, string folder, string fileName);

        Task<AuthenticationResult> Authenticate(string authority, string resource, string clientId, string returnUri);
        void ClearAllCookies();

    }
}
