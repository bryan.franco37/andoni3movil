﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndonAlarm.Common
{
    public class Message
    {
        public bool Value { get; set; }
    }

    public class ProductionLineDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Operator { get; set; }
        public AndonAlert Alert { get; set; }

    }

    //public class ProductionLineXamarin
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Operator { get; set; }
    //    public AndonAlertDto Alert { get; set; }

    //}

    public class AndonAlert
    {
        public int AndonAlertId { get; set; }
        public string UserId { get; set; }
        //public virtual User User { get; set; }
        public int PpmAddressId { get; set; }
        public int AndonAlertTypeId { get; set; }
        public virtual AndonAlertType AndonAlertType { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;


        public int DrawAndonAlertTypeId
        {
            get
            {
                if (AndonAlertTypeId == 2 && (DateTime.UtcNow - CreatedAt).TotalSeconds > 7200)
                {
                    return 1;
                }
                else
                {
                    return AndonAlertTypeId;
                }
            }
        }

        public int AlarmDelaySeconds
        {
            get
            {
                return (int)Math.Round((CreatedAt.AddSeconds(7200) - DateTime.UtcNow).TotalSeconds, 0);
            }
        }

    }

    //public class AndonAlertDto
    //{
    //    public int AndonAlertId { get; set; }
    //    public int DrawAndonAlertTypeId { get; set; }
    //    public int AlarmDelaySeconds { get; set; }

    //    //public string UserId { get; set; }
    //    ////public virtual User User { get; set; }
    //    //public int PpmAddressId { get; set; }
    //    //public int AndonAlertTypeId { get; set; }
    //    ////public virtual AndonAlertType AndonAlertType { get; set; }
    //    //public DateTime CreatedAt { get; set; } = DateTime.UtcNow;


    //    //private readonly int DelaySeconds = 60;

    //    //public int DrawAndonAlertTypeId
    //    //{
    //    //    get
    //    //    {
    //    //        if (AndonAlertTypeId == 2 && (DateTime.UtcNow - CreatedAt).TotalSeconds > DelaySeconds)
    //    //        {
    //    //            return 1;
    //    //        }
    //    //        else
    //    //        {
    //    //            return AndonAlertTypeId;
    //    //        }
    //    //    }
    //    //}

    //    //public int AlarmDelaySeconds
    //    //{
    //    //    get
    //    //    {
    //    //        return (int)Math.Round((CreatedAt.AddSeconds(DelaySeconds) - DateTime.UtcNow).TotalSeconds, 0);
    //    //    }
    //    //}

    //}

    public class AndonAlertType
    {
        public int AndonAlertTypeId { get; set; }
        public string Name { get; set; }
    }

    public class AlertDto
    {
        public int ProductionLineId { get; set; }
        public int TypeId { get; set; }
        public string Comment { get; set; }
    }

    public class CredentialsDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
