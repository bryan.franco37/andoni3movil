﻿
namespace AndonAlarm.Common
{
    class Constants
    {
        public const string ClientId = "c7b57cb5-76f6-4241-acd3-dd7289dd5cae";
        public static string Authority = "https://login.microsoftonline.com/03fa6430-2a3b-4a59-b916-ffb7eb0b395c";
        public const string ReturnUri = "http://localhost";
        public const string GraphResourceUri = "https://graph.windows.net";

        public const string Url = "https://192.168.30.91:4646";
        //public const string Url = "http://10.38.160.10:4545";
        //public const string Url = "http://192.168.28.31/InternalRequisition";
    }
}
