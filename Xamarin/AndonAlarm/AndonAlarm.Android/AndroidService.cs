﻿using Android.App;
using Android.Webkit;
using Java.IO;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using AndonAlarm.Common;
using System.Threading.Tasks;
using System;
using System.Linq;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndonAlarm.Droid.Helper.AndroidService))]
namespace AndonAlarm.Droid.Helper
{
    public class AndroidService : IAndroidService
    {

        string IAndroidService.GetExternalFilesPath()
        {
            return Android.App.Application.Context.GetExternalFilesDir(null).AbsolutePath;
        }
        async Task IAndroidService.SaveImage(byte[] reducedImage, string folder, string fileName)
        {
            
            var filename = System.IO.Path.Combine(folder, fileName);
            using (var fileOutputStream = new FileOutputStream(filename))
            {
                await fileOutputStream.WriteAsync(reducedImage);
            }
        }

        async Task<AuthenticationResult> IAndroidService.Authenticate(string authority, string resource, string clientId, string returnUri)
        {
            var authContext = new AuthenticationContext(authority);
            if (authContext.TokenCache.ReadItems().Any())
                authContext = new AuthenticationContext(authContext.TokenCache.ReadItems().First().Authority);
            var uri = new Uri(returnUri);
#pragma warning disable CS0618 // Type or member is obsolete
            var platformParams = new PlatformParameters((Activity)Forms.Context);
#pragma warning restore CS0618 // Type or member is obsolete
            var authResult = await authContext.AcquireTokenAsync(resource, clientId, uri, platformParams);
            return authResult;
        }
        void IAndroidService.ClearAllCookies()
        {
            CookieManager cookieManager = CookieManager.Instance;
            cookieManager.RemoveAllCookie();
            cookieManager.Flush();
        }

    }
}