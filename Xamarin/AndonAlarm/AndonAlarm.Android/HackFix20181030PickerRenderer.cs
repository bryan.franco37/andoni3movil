﻿using AndonAlarm.Droid;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(Picker), typeof(HackFix20181030PickerRenderer))]
namespace AndonAlarm.Droid
{
    public class HackFix20181030PickerRenderer : Xamarin.Forms.Platform.Android.AppCompat.PickerRenderer
    {
        public HackFix20181030PickerRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Focusable = false;
            }
        }
    }
}